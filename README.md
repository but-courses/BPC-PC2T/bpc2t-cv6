# Course: BPC2T - seminar 6
This project provides a solution for seminar 6. Includes topics like:
1. Class and main method creation 
2. Reading and Writing to the text file (BufferedReader/BufferedWriter, ...)
3. Serialization of objects (ObjectOutputStream, ...)
4. Using String.format parameters
5. Using Scanner class to read console input
6. Distinguish between text and binary files.

## Authors

email | Name 
------------ | -------------
pavelseda@email.cz | Šeda Pavel

## Starting up Project
Just import it in your selected IDE (Eclipse, IntelliJ IDEA, Netbeans, ...)

## Used Technologies
The project was built and tested with these technologies, so if you have any unexpected troubles let us know.

```
Maven         : 3.3.9
Java          : OpenJDK 11
```