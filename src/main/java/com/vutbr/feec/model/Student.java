package com.vutbr.feec.model;

import java.io.Serializable;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Student implements Serializable {

	private long id;
	private String name;

	public Student(long id, String name) {
		super();
		setId(id);
		setName(name);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + "]";
	}

}
