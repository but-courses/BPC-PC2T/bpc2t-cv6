package com.vutbr.feec;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import com.vutbr.feec.io.CopyGivenFile;
import com.vutbr.feec.io.PrintUtils;
import com.vutbr.feec.io.ReadFromFile;
import com.vutbr.feec.io.WriteObjectWithSerialization;
import com.vutbr.feec.model.Student;

/**
 * @author Pavel Seda
 */
public class App {

    private static final String MAVEN_RESOURCES_PREFIX = "./src/main/resources/";

    public static void main(String[] args) {
        File persons = new File(MAVEN_RESOURCES_PREFIX + "persons.csv");

        ReadFromFile readPersonsFile = new ReadFromFile();
        String fileContent = readPersonsFile.readFile(persons);
        System.out.println(fileContent);

//		WriteToFile writePersonToFile = new WriteToFile();
//		writePersonToFile.appendToFile(persons, "55,\"Pavel Seda\",pavelseda@email.cz,2100-01-01,SedaQ,88");

        WriteObjectWithSerialization writeStudent = new WriteObjectWithSerialization();
        writeStudent.serialize(new File(MAVEN_RESOURCES_PREFIX + "students"), new Student(1, "Pavel Seda"));


        // copy file from one location to another
        Path imageSource = Paths.get(MAVEN_RESOURCES_PREFIX + "vut-logo.jpg");
        Path imageTarget = Paths.get(MAVEN_RESOURCES_PREFIX + "vut-logo2.jpg");

        CopyGivenFile copyGivenFile = new CopyGivenFile();
        copyGivenFile.copyFile(imageSource, imageTarget);

        PrintUtils printUtils = new PrintUtils();
        printUtils.printMatrix();
        printUtils.printFolderContent();
        printUtils.printFolderContentIncludingSubdirectories();
    }

}
