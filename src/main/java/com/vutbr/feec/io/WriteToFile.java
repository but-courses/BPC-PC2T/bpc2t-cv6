package com.vutbr.feec.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author Pavel Seda
 *
 */
public class WriteToFile {

	public void appendToFile(File file, String strToAppend) {
		// the second parameter of FileWriter (true) turns on "append" mode
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {
			bw.write(System.lineSeparator() + strToAppend);
			bw.flush();
		} catch (IOException e) {
			// handle exception ... (log or rethrow)
			throw new RuntimeException("Cannot write to file.." + e);
		}
	}
}
