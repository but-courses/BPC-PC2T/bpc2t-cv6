package com.vutbr.feec.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 * @author Pavel Seda
 */
public class CopyGivenFile {

    public void copyFile(Path source, Path target) {
        try {
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException io) {
            new RuntimeException("It is not possible to copy given file..", io);
        }
    }
}
