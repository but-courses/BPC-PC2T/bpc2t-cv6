package com.vutbr.feec.io;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * @author Pavel Seda
 */
public class PrintUtils {

    /**
     * Vytiskne dvourozmernou matici, matice bude ma vhodne odsazene hodnoty
     */
    public void printMatrix() {
        int[][] matrix = new int[3][3];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.printf("%4d", matrix[i][j]);
            }
            System.out.println("");
        }
    }

    public void printFolderContent() {
        File directory = null;
        do {
            System.out.println("Zadejte název adresáře (absolutní cestu), z kterého chcete zobrazit složky.");
            System.out.println("Adresář musí být existující složka ve file systému.");

            Scanner sc = new Scanner(System.in);
            String directoryPath = sc.nextLine();
            directory = new File(directoryPath);
        } while (!directory.exists());
        File[] directoryContent = directory.listFiles();
        for (int i = 0; i < directoryContent.length; i++) {
            System.out.println(directoryContent[i].getName());
        }
    }

    /**
     * Tato metoda využívá funkcionální programování a třídu Path z java.nio (tyto
     * záležitosti jsou nad rámec kurzu BPC2T).
     */
    public void printFolderContentIncludingSubdirectories() {
        Path directory = null;
        do {
            System.out.println("Zadejte název adresáře (absolutní cestu), z kterého chcete zobrazit složky.");
            System.out.println("Adresář musí být existující složka ve file systému.");
            Scanner sc = new Scanner(System.in);
            String directoryPath = sc.nextLine();
            directory = Paths.get(directoryPath);
        } while (Files.notExists(directory));
        try {
            Files.walk(directory).filter(Files::isRegularFile).forEach(System.out::println);
        } catch (IOException e) {
            // handle exception ... (log or rethrow)
            throw new RuntimeException("Cannot print directories and sub-directories.." + e);
        }

    }

}
