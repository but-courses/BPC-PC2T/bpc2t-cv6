package com.vutbr.feec.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.vutbr.feec.model.Student;

/**
 * 
 * @author Pavel Seda
 *
 */
public class WriteObjectWithSerialization {

	public void serialize(File file, Student student) {
		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
			out.writeObject(student);
		} catch (IOException e) {
			// handle exception ... (log or rethrow)
			throw new RuntimeException("Cannot serialize to file.." + e);
		}
	}
}
