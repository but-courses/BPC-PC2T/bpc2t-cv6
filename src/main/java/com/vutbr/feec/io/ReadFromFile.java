package com.vutbr.feec.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 * @author Pavel Seda
 *
 */
public class ReadFromFile {

	public String readFile(File file) {
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = "";
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			return sb.toString();
		} catch (IOException e) {
			// handle exception ... (log or rethrow)
			throw new RuntimeException("Cannot read file.." + e);
		}
	}
}
