1. Vysvětlete jaké typy souborů existují?
   Pracujeme vždy pouze s textovými soubory?
      - ne, pracujeme i s binárními soubory (například obrázky)
   Jak pracujeme se soubory, které nejsou textové?
      - Tyto soubory čteme po blocích bytů (např. 1024)
   Uveďte příklad netextového souboru.
      - jpeg., png., gif. a mnoho dalších